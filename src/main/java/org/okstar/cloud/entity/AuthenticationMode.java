package org.okstar.cloud.entity;

public enum AuthenticationMode {
	BASIC_AUTH, SHARED_SECRET_KEY
}
